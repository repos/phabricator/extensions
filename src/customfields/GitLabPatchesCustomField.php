<?php

/**
 * A GitLab merge request widget.
 *
 * (Also known as Mr. Widget.)
 *
 * Hits the GitLab search API and looks for merge request descriptions with
 * a bug trailer of the form:
 *
 * Bug: T1234
 *
 * Displays these in a table similar to the one provided for Gerrit patches.
 */
class GitLabPatchesCustomField
  extends ManiphestCustomField
{
  // Set this to a specific task ID to test against that task:
  private $task_override = false; // "324164";

  public function getFieldKey() {
    return 'wmfgitlab:patches';
  }

  public function getModernFieldKey() {
    return 'gitlab.patches';
  }

  public function getFieldName() {
    return pht('GitLab Patches');
  }

  public function shouldAppearInApplicationSearch() {
    return false;
  }

  public function shouldAppearInApplicationTransactions() {
    return false;
  }

  public function shouldAppearInConduitTransactions() {
    return false;
  }

  public function shouldAppearInConduitDictionary() {
    return false;
  }

  public function shouldAppearInEditEngine() {
    return false;
  }

  public function shouldAppearInEditView() {
    return false;
  }

  public function shouldAppearInPropertyView() {
    $taskid = $this->getTaskId();
    if (!$taskid) {
      return false;
    }
    try {
      $mr_for_task = $this->getMergeRequestsForTask($taskid);
    } catch (Exception $e) {
      $mr_for_task = '';
    }
    return !empty($mr_for_task);
  }

  protected function getTaskId() {
    if ($this->task_override && PhabricatorEnv::getEnvConfig('phabricator.developer-mode')) {
      return $this->task_override;
    } else {
      $task = $this->getObject();
      if (!$task) {
        return false;
      }
      return $task->getID();
    }
  }

  public function renderPropertyViewLabel() {
    pht("Related Changes in GitLab:");
  }

  public function getStyleForPropertyView() {
    return 'block';
  }

  public function renderPropertyViewValue(array $handles) {
    $taskid = $this->getTaskId();
    try {
      $mr_for_task = $this->getMergeRequestsForTask($taskid);
    } catch (Exception $e) {
      return '';
    }
    return $mr_for_task;
  }

  /**
   * Returns a PHUIStatusListView for merge requests which mention a task.
   *
   * Takes a numeric task id, queries for trailers matching the Tnnn format.
   */
  private function getMergeRequestsForTask($taskid) {
    // If a second format is needed here, you can array_merge() multiple calls
    // to getMergeRequestsForSearch().
    $changes = $this->getMergeRequestsForSearch('"Bug: T' . $taskid . '"');
    if (! is_array($changes)) {
      return '';
    }

    $view = new PHUIStatusListView();
    Javelin::initBehavior('phabricator-tooltips');

    $rows = array();
    foreach($changes as $change) {
      if (! is_array($change)) {
        continue;
      }

      // Ensure the change contains all of the expected fields, so we never
      // blow up on accessing an undefined index:
      $all_values_present_and_accounted_for = isset(
        $change['state'],
        $change['web_url'],
        $change['title'],
        $change['references']['full'],
        $change['source_branch'],
        $change['target_branch'],
        $change['author']['web_url'],
        $change['author']['username']
      );
      if (! $all_values_present_and_accounted_for) {
        continue;
      }

      // Set up an icon for the MR's current state.
      // State is one of:  opened, closed, merged, locked
      // See GitLab's app/models/merge_request.rb
      $icons = array();
      $status = $change['state'];
      $icon = 'fa-clock-o blue';
      $icon_title = pht($status);
      if ($status == 'merged') {
        $icon = 'fa-code-fork green';
      } elseif ($status == 'closed') {
        $icon = 'fa-close red';
      } elseif ($status == 'locked') {
        $icon = 'fa-lock black';
      }
      $icons[] = id(new PHUIIconView())
        ->setIcon($icon)
        ->addSigil('has-tooltip')
        ->setMetadata(
          array(
            'tip' => $icon_title,
            'size' => 240,
          ));

      $url = $change['web_url'];
      $title = $change['title'];

      $rows[] = array(
        $icons,
        phutil_tag('a', array('href' => $url), $title),
        $change['references']['full'],
        phutil_tag('a', array('href' => $change['author']['web_url']), $change['author']['username']),
        $change['source_branch'],
        $change['target_branch'],
      );
    }

    $changes_table = id(new AphrontTableView($rows))
      ->setHeaders([
        '',
        'Title',
        'Reference',
        'Author',
        'Source Branch',
        'Dest Branch',
      ])
      ->setNoDataString(pht('This task has no related GitLab merge requests.'))
      ->setColumnClasses(
        array(
          'right',
          'wide',
          'left',
          'left',
          'left',
          'left',
        ))
      ->setColumnVisibility(
        array(
          true,
          true,
          true,
          true,
          true,
        ))
      ->setDeviceVisibility(
        array(
          true,
          true,
          false,
          true,
          false,
        ));

    if (empty($rows)) {
      return '';
    }
    $gitlab_search = 'https://gitlab.wikimedia.org/search?scope=merge_requests&search='
      . urlencode('"Bug: T' . $taskid . '"');
    $search_link = phutil_tag('a', ['href' => $gitlab_search],
      pht('Customize query in GitLab')
    );
    if (count($rows) > 10) {
      require_celerity_resource('wikimedia-extensions-css');
      $table_id = celerity_generate_unique_node_id();
      $link_id = celerity_generate_unique_node_id();
      $changes_div = phutil_tag('div', [
        "id" => $table_id,
        "class" => "gerrit-patches-hidden",
      ], $changes_table);

      $toggle_link = javelin_tag('a', [
          "id" => $link_id,
          "class" => "button button-grey",
          "sigil" => 'jx-toggle-class',
          "meta" => [
            "map" => [
              $table_id => "gerrit-patches-expanded",
              $link_id => 'gerrit-button-hidden',
            ]
          ]
        ], pht('Show related patches')
      );

      return array($changes_div, $toggle_link, " ", $search_link);
    }
    return array($changes_table, $search_link);
  }

  /**
   * Search in the merge request scope for a given string.
   *
   * Here be cache dragons.
   */
  private function getMergeRequestsForSearch($search) {
    $search_param = urlencode($search);
    $gitlab_url = "https://gitlab.wikimedia.org/api/v4/search?scope=merge_requests&search=$search_param";

    // This must be set in local.json in prod:
    $GITLAB_API_KEY = PhabricatorEnv::getEnvConfigIfExists('gitlab.api_key');
    if ($GITLAB_API_KEY === null || $GITLAB_API_KEY === '') {
      return array();
    }

    $cache = PhabricatorCaches::getMutableCache();
    $cachekey = 'gitlab:mergereqs:bloop' . $search;
    $changes = $cache->getKey($cachekey, null);

    if ($changes === "loading") {
      // Data is being loaded by another request / process, avoid hammering GitLab:
      return array();
    }

    if (! $changes) {
      // Attempt to avoid thundering herd - lock the cache by setting it to
      // "loading" while https request is pending, timeout the cache entry in 5
      // seconds:
      $cache->setKey($cachekey, "loading", 5);

      // Request the list of changes from GitLab:
      try {
        $future = new HTTPSFuture($gitlab_url);
        $future->setTimeout(4); // 4 seconds - default here is 300
        $future->addHeader('PRIVATE-TOKEN', $GITLAB_API_KEY);
        list($status, $changes, $headers) = $future->resolve();
      } catch (Exception $e) {
        return array();
      }

      // Make sure we got an OK from the request:
      if ($status->getStatusCode() !== 200 || $changes === false) {
        $cache->setKey($cachekey, '', 10);
        return array();
      }

      if ($changes === '') {
        return array();
      }
      $cache->setKey($cachekey, $changes, 600);
    }

    $decoded_changes = json_decode($changes, true);
    if (is_array($decoded_changes)) {
      return $decoded_changes;
    }
    return array();
  }
}
