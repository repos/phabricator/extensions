<?php
/**
 * Static utility functions for dealing with projects and policies.
 * These are used by our custom task policy extension
 * (SecurityPolicyEnforcerAction) and some other code to
 * avoid code duplication.
 */
final class WMFSecurityPolicy
{
  /**
   * look up a project by name
   * @param array|string $projectName
   * @return PhabricatorProject|null
   */
  public static function getProjectByName($projectName, $viewer=null, $needMembers=false) {
    if ($viewer === null) {
      $viewer = PhabricatorUser::getOmnipotentUser();
    }
    if (!is_array($projectName)) {
      $projectName = array($projectName);
    }
    $query = new PhabricatorProjectQuery();
    $query->setViewer($viewer)
              ->withNames($projectName)
              ->needMembers($needMembers);
    if (count($projectName) == 1) {
      return $query->executeOne();
    } else {
      return $query->execute();
    }
  }

  /**
   * Creates a custom policy for the given task having the following properties:
   *
   * 1. The users listed in $user_phids can view/edit
   * 2. Members of the project(s) in $project_phids can view/edit
   * 3. $task Subscribers (aka CCs) can view/edit
   *
   * @param ManiphestTask $task - the task the policy will apply to
   * @param array $user_phids - phids of users who can view/edit
   * @param array $project_phids - phids of projects who's members can view/edit
   * @param bool $include_subscribers - determine if subscribers can view/edit
   * @param PhabricatorPolicy $old_policy - if supplied, update the rules to an existing policy
   * @param boolean $save - save the policy to db storage?
   */
  public static function createCustomPolicy(
    $task,
    $user_phids,
    $project_phids,
    $include_subscribers=true,
    $old_policy=null,
    $save=true) {

    if (!is_array($user_phids)) {
      $user_phids = array($user_phids);
    }
    if (!is_array($project_phids)) {
      $project_phids = array($project_phids);
    }

    $policy = $old_policy instanceof PhabricatorPolicy
            ? $old_policy
            : new PhabricatorPolicy();

    $rules = array();
    if (!empty($user_phids)){
      $rules[] = array(
        'action' => PhabricatorPolicy::ACTION_ALLOW,
        'rule'   => 'PhabricatorUsersPolicyRule',
        'value'  => $user_phids,
      );
    }
    if (!empty($project_phids)) {
      $rules[] = array(
        'action' => PhabricatorPolicy::ACTION_ALLOW,
        'rule'   => 'PhabricatorProjectsPolicyRule',
        'value'  => $project_phids,
      );
    }
    if ($include_subscribers) {
      $rules[] = array(
        'action' => PhabricatorPolicy::ACTION_ALLOW,
        'rule'   => 'PhabricatorSubscriptionsSubscribersPolicyRule',
        'value'  => array($task->getPHID()),
      );
    }

    $policy
      ->setRules($rules)
      ->setDefaultAction(PhabricatorPolicy::ACTION_DENY);
    if ($save)
      $policy->save();
    return $policy;
  }

  public static function isTaskPublic($task) {
    $policy = $task->getViewPolicy();

    $public_policies = array(
       PhabricatorPolicies::POLICY_PUBLIC,
       PhabricatorPolicies::POLICY_USER);

    return in_array($policy, $public_policies);
  }


  public static function userCanLockTask(PhabricatorUser $user, ManiphestTask $task) {
    if (!$user->isLoggedIn()) {
      return false;
    }
    $user_phid = $user->getPHID();
    $author_phid = $task->getAuthorPHID();
    if ($user_phid == $author_phid) {
      return true;
    }
    $trusted_project_names = ["Trusted-Contributors", "WMF-NDA", "acl*sre-team", "acl*security"];
    $projects = self::getProjectByName($trusted_project_names, $user, true);

    foreach ($projects as $proj) {
      try {
        if ($proj instanceof PhabricatorProject &&
            $proj->isUserMember($user_phid)) {
          return true;
        }
      } catch(PhabricatorDataNotAttachedException $e) {
        continue;
      }
    }

    return false;
  }

}
