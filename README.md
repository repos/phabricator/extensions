# About this repository

This repository contains miscellaneous extensions to Phabricator which are
specialized for the needs of the Wikimedia Foundation's Phabricator instance
at https://phabricator.wikimedia.org

These extensions provide some basic custom functionality and integration with
Wikimedia's systems.

## Installation


This Repository consists of a single libphutil module which can be used in
phabricator by simply adding the repository root to the list of library paths
specified by the key `load-libraries` within phabricator's config.

### For example:

```json
"load-libraries": [
  "/path/to/this/repository/",
  "/path/to/another/extension/"
]
```

For more details, see [this article](https://we.phorge.it/book/contrib/article/adding_new_classes/#linking-with-phorge) in the Phorge documentation.

## Overview of extensions

The extensions are under the `src/` directory, organized into sub-directories
by extension type.

### src/oauth

`PhabricatorMediaWikiAuthProvider` and `PhutilMediaWikiAuthAdapter` constitute
an `authentication provider adapter` that enables Phabricator to use OAuth
federation to offload phabricator logins to Mediawiki's OAuth1 endpoint.

### src/customfields

Custom fields are extensions which add a field to various objects in
Phabricator. Wikimedia makes use of a few custom fields to extend user profile
pages and Differential code review pages.

#### `MediaWikiUserpageCustomField`
This custom field is used on phabricator user profile
pages, displays a link to a user's wiki userpage. The wiki userpage url is
discovered by looking up the link which is created by
`PhabricatorMediaWikiAuthProvider` when a user links their mediawiki login to
their phabricator account.

#### `LDAPUserpageCustomField`
Another custom field used on phabricator user profile pages
which simply displays the ldap username that is associated with the user's
phabricator account.

### src/gerrit

Implements a custom commit field for Change-Id's.

### Security Policy Extensions

See [src/policy/README](https://phabricator.wikimedia.org/source/phab-extensions/browse/wmf%252Fstable/src/policy/)

### Transaction Rollback

There is a command line tool in bin/rollback which can be used to roll back transactional edits on Maniphest tasks. Other phabricator applications could be supported with a little effort, however, none are supported currently. See `src/workflow/RollbackTransactionsWorkflow.php`
